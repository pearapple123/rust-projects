use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let greeting_file = File::open("hello.txt").unwrap_or_else(|error| {
        match error.kind() {
            ErrorKind::NotFound => File::create("hello.txt").unwrap_or_else(|error| panic!("Problem creating the file: {:?}", error)),
            _ => panic!("Problem opening the file: {:?}", error)
        }
    });
}
