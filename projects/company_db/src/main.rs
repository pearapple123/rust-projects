use std::io;
use std::collections::HashMap;
use std::process::exit;

fn append_to_db(db: &mut HashMap<String, String>) -> () {
    let mut employee = String::new();
    let mut dept = String::new();

    // grab employee name and dept from stdin
    println!("Enter employee below:");
    io::stdin().read_line(&mut employee)
        .expect("Failed to read employee");
    let employee = employee.trim();

    println!("Enter their department below:");
    io::stdin().read_line(&mut dept)
        .expect("Failed to read department");
    let dept = dept.trim();

    db.insert(employee.to_string(), dept.to_string());
}

fn list_db_by_dept(db: &HashMap<String, String>) -> () {
    // generate a "reverse" hashmap with departments as keys
    // and vectors of employees as vals
    let mut reverse_db: HashMap<String, Vec<String>> = HashMap::new();

    for (employee, dept) in db {
        // grab corresponding vector of employees given dept.
        // if it doesn't exist, create a record for that dept
        // giving it an empty vector of employees
        let dept_employees = reverse_db.entry(dept.to_string()).or_insert(vec![]);
        dept_employees.push(employee.to_string());
    }

    for (dept, ls) in reverse_db {
        println!("{dept}: {ls:?}");
    }
}

fn main() {
//    let mut db: HashMap<String, String> = HashMap::new();
    let mut db = HashMap::from([
            ("J P".to_string(), "Finance".to_string()),
            ("C J".to_string(), "Finance".to_string())
    ]);

    // program loop
    loop {

        println!("\nType \"a\" to append to the company db or \"l\" to list db by department or \"q\" to quit");

        let mut input = String::new();

        io::stdin().read_line(&mut input)
            .expect("Failed to read line");

        let input = input.trim();

        match input {
            "a" => append_to_db(&mut db),
            "l" => list_db_by_dept(&db),
            "q" => exit(0),
            _ => continue
        }

    }
}
