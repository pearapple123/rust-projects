// Simulating discharge of a capacitor w/ iterative modelling

// use SI prefixes for the args
fn cap_sim(resistance: f32, capacitance: f32, init_charge: f32, time_interval: f32) {

    let time_const: f32 = resistance * capacitance;

//    println!("Time\tCharge");

    let mut charge: f32 = init_charge;
    let mut time: f32 = 0.0;

    // break when charge stored is less than 37% of the initial charge
    // the time at which this occurs is an estimation of the time constant
    while charge >= (0.37 * init_charge) {
	
	// prints both vars to 3dp
	//	println!("{time:.3}\t{charge:.3}");


	// iterative step:
	// change in charge = (Q/RC) * time interval
	let delta_charge: f32 = charge * time_interval / time_const;
	
	// updating variables
	charge -= delta_charge;
	time += time_interval;
	
    }

    println!("------------------------------------------");
    println!("Time interval: {time_interval:.4}");
    println!("Estimation for time constant: {time:.4} s");
    println!("Percentage error: {:.2}", 100.0 * (10.0 - time) / time);
    println!("------------------------------------------");
}

fn main() {

    let time_ints: [f32; 5] = [0.001, 0.005, 0.01, 0.05, 0.1];
    
    for t in time_ints { cap_sim( 10000.0, 0.001, 100.0, t ); }
    
}
