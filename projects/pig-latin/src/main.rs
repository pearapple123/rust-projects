// Pig latin

fn main() {
    let sentence = "Hello world apple";
    // split string on spaces
    let words: Vec<&str> = sentence.split_whitespace().collect();

    let mut pigged_words: Vec<String> = Vec::new();

    // maps to_pig to each word in words vec
    for w in words {
        pigged_words.push(to_pig(w).to_lowercase());
    }

    let mut pigged_sentence = String::new();

    // re-forms sentence
    for w in pigged_words {
        let s = format!("{w} ");
        pigged_sentence.push_str(&s);
    }

    // there's a space at the end, trim removes it
    let pigged_sentence = pigged_sentence.trim();

    println!("{pigged_sentence}");
}

fn to_pig(s: &str) -> String {
    let head: &str = &s[0..1];

    match head {
        "a" | "e" | "i" | "o" | "u" => format!("{s}-hay"),
        _ => format!("{}-{head}ay", &s[1..])
    }
}
