use std::io;
use std::cmp::Ordering;
use rand::Rng;

fn main() {

    println!("Guess the number!");

    // generate secret number
    let secret_number = rand::thread_rng().gen_range(1..=100);

    loop {

	println!("Please input your guess.");

	// declaring mutable variable guess
	// new() is an associated func of String type, creating
	// empty string
	let mut guess = String::new();
	
	// read_line() calls read_line on stdin and stores input
	// in `guess`
	// `&guess` is a reference to the variable guess, allows
	// you to access the variable w/out copying the variable
	// into mem many times
	// read_line() returns Enum type, which can either be Ok
	// or Err. Ok indicates success and contains the input,
	// Err contains error msg. We use the expect() method on
	// the Result type to display error msg if Result is Err
	// If Ok, expect just passes input data through
	io::stdin()
	    .read_line(&mut guess)
	    .expect("Failed to read line");

	// trim() removes whitespaces at the beginning and end of
	// the string. parse() converts string to a number,
	// specifically u32 as that's the type of `guess`. It
	// returns a Result type so we use expect() for err handl
	// ing.
	// Alternatively, use a match exp and match for Ok & Err
	let guess: u32 = match guess.trim().parse() {
	    Ok(num) => num,
	    Err(_) => continue
	};
	
	println!("You guessed: {guess}");

	/*    match guess.cmp(&secret_number) {
	Ordering::Less => println!("Too small!"),
	Ordering::Greater => println!("Too big!"),
	Ordering::Equal => println!("You win!")
    }

	doesn't compile as secret_number is a number but `guess`
	is a string, so we can't compare them.
	 */

	match guess.cmp(&secret_number) {
	    Ordering::Less => println!("Too small!"),
	    Ordering::Greater => println!("Too big!"),
	    Ordering::Equal => {
		println!("You win!");
		break;
	    }
	}
	
    }
}
