use std::io;
use std::cmp::Ordering;
use rand::Rng;

struct Guess {
    value: u32
}

impl Guess {
    fn new(value: u32) -> Guess {
        if value < 1 || value > 100 {
            panic!("The number must be between 1 and 100 inclusive, you entered {value}");
        }

        Guess { value }
    }

    fn get(&self) -> u32 {
        self.value
    }
}

fn main() {

    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..=100);
    let mut tries: u32 = 0;

    loop {
        tries += 1;
    
    	println!("Please input a number between 1 and 100 inclusive.");
    
    	let mut guess = String::new();
    	
    	io::stdin()
    	    .read_line(&mut guess)
    	    .expect("Failed to read line");
    
    	let guess: Guess = match guess.trim().parse() {
    	    Ok(num) => Guess:new(num),
    	    Err(_) => continue
    	};
    	
    	println!("You guessed: {}", guess.get());
    
    	match guess.get().cmp(&secret_number) {
    	    Ordering::Less => println!("Too small!"),
    	    Ordering::Greater => println!("Too big!"),
    	    Ordering::Equal => {
    		println!("You win!");
    		println!("You took {tries} tries to win.");
    		break;
    	    }
	    }
	
    }
}
