#[derive(Debug)]
struct Person {
    name: String,
    age: u32
}

impl Person {
    fn update_age(&mut self) { self.age += 1 }
}

fn main() {
    let mut me = Person {
        name: String::from("Hendrik"),
        age: 20
    };

    println!("Me: {me:?}");

    me.update_age();

    println!("Me: {me:?}");
}
